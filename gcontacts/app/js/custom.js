var gId, zId,etag, entityId;
function initializeWidget()
{
	ZOHO.embeddedApp.on("PageLoad",function(data)
	{
		entityId = data.EntityId[0];
		if(data && data.Entity)
		{
			ZOHO.CRM.API.getRecord({Entity:data.Entity,RecordID:data.EntityId})
			.then(function(response)
			{
						$("#z-fname").val(response.data[0].First_Name);
						$("#z-lname").val(response.data[0].Last_Name);
						$("#z-mobile").val(response.data[0].Mobile);
						$("#z-phone").val(response.data[0].Phone);
						$("#z-email").val(response.data[0].Email);
						$("#z-dob").val(response.data[0].Date_of_Birth);
						$("#z-street").val(response.data[0].Mailing_Street);
						$("#z-state").val(response.data[0].Mailing_State);
						$("#z-city").val(response.data[0].Mailing_City);
						$("#z-country").val(response.data[0].Mailing_Country);
						$("#z-zipcode").val(response.data[0].Mailing_Zip);
						gId = "people/"+response.data[0]["synccontacts.gId"];
						zId = response.data[0].id;
			});
			ZOHO.CRM.CONNECTOR.invokeAPI("synccontacts.gcontacts.importcontacts",data)
			.then(function(data){
					if(data.status_code == 200){
						var response = JSON.parse(data.response);
						var i=0; limit = response.totalPeople;
						for (var connections in response.connections[i]){
								if(i<limit && response.connections[i].resourceName==gId){
										etag = response.connections[i].etag;
										$("#g-fname").val((response.connections[i].names[0].givenName)?response.connections[i].names[0].givenName:"");
										$("#g-lname").val((response.connections[i].names[0].familyName)?response.connections[i].names[0].familyName:"");
										$("#g-mobile").val((response.connections[i].phoneNumbers[0])?response.connections[i].phoneNumbers[0].value:"");
										$("#g-phone").val((response.connections[i].phoneNumbers[1])?response.connections[i].phoneNumbers[1].value:"");
										if(response.connections[i].birthdays){
											let d = response.connections[i].birthdays[0].date;
											$("#g-dob").val(d.year+"-"+d.month+"-"+d.day);
										}
										$("#g-email").val((response.connections[i].emailAddresses)?response.connections[i].emailAddresses[0].value:"");
										if(response.connections[i].addresses){
											$("#g-street").val((response.connections[i].addresses[0].streetAddress)?response.connections[i].addresses[0].streetAddress:"");
											$("#g-state").val((response.connections[i].addresses[0].region)?response.connections[i].addresses[0].region:"");
											$("#g-city").val((response.connections[i].addresses[0].city)?response.connections[i].addresses[0].city:"");
											$("#g-country").val((response.connections[i].addresses[0].country)?response.connections[i].addresses[0].country:"");
											$("#g-zipcode").val((response.connections[i].addresses[0].postalCode)?response.connections[i].addresses[0].postalCode:"");
										}
									}
									i++;
						}
					}
					else{
						alert("Google api not connected...\nTry again later")
					}
			});
		}
	});
	ZOHO.embeddedApp.init();
}
function push_data(arg){

	$("#fname").val($("#"+arg+"fname").val());
	$("#lname").val($("#"+arg+"lname").val());
	$("#mobile").val($("#"+arg+"mobile").val());
	$("#phone").val($("#"+arg+"phone").val());
	$("#email").val($("#"+arg+"email").val());
	$("#dob").val($("#"+arg+"dob").val());
	$("#street").val($("#"+arg+"street").val());
	$("#state").val($("#"+arg+"state").val());
	$("#city").val($("#"+arg+"city").val());
	$("#country").val($("#"+arg+"country").val());
	$("#zipcode").val($("#"+arg+"zipcode").val());

}
function mergeContact(){

	$("#fname").val(($("#z-fname").val() == $("#g-fname").val())?actionTrue("fname"):actionFalse("fname"));
	$("#lname").val(($("#z-lname").val() == $("#g-lname").val())?actionTrue("lname"):actionFalse("lname"));
	$("#mobile").val(($("#z-mobile").val() == $("#g-mobile").val())?actionTrue("mobile"):actionFalse("mobile"));
	$("#phone").val(($("#z-phone").val() == $("#g-phone").val())?actionTrue("phone"):actionFalse("phone"));
	$("#email").val(($("#z-email").val() == $("#g-email").val())?actionTrue("email"):actionFalse("email"));
	$("#dob").val(($("#z-dob").val() == $("#g-dob").val())?actionTrue("dob"):actionFalse("dob"));
	$("#street").val(($("#z-street").val() == $("#g-street").val())?actionTrue("street"):actionFalse("street"));
	$("#state").val(($("#z-state").val() == $("#g-state").val())?actionTrue("state"):actionFalse("state"));
	$("#city").val(($("#z-city").val() == $("#g-city").val())?actionTrue("city"):actionFalse("city"));
	$("#country").val(($("#z-country").val() == $("#g-country").val())?actionTrue("country"):actionFalse("country"));
	$("#zipcode").val(($("#z-zipcode").val() == $("#g-zipcode").val())?actionTrue("zipcode"):actionFalse("zipcode"));

}

function actionTrue(arg){
	if($("#z-"+arg).val()!="")		return $("#z-"+arg).val();
	else{
		$("#add-z-"+arg).hide();
		$("#add-g-"+arg).hide();
		return "";
	}
}

function actionFalse(arg) {
	($("#z-"+arg).val()!="")?$("#add-z-"+arg).show():$("#add-z-"+arg).hide();
	($("#g-"+arg).val()!="")?$("#add-g-"+arg).show():$("#add-g-"+arg).hide();


	return "";
}
/*
function init() {
	var x = ["fname","lname","mobile","phone","email","dob","street","state","city","country","zipcode"];
	x.forEach(function(ele){
			$("#add-z-"+ele).style.display = "none";
			$("#add-g-"+ele).style.display = "none";
	})
}
*/

function addField(arg1,arg2) {
	$("#"+arg2).val($("#"+arg1+arg2).val());
}

function updateFields() {
	//update zoho crm contact by invoking api
	var param;
	let fname = $("#fname").val();
	let lname = $("#lname").val();
	let mobile = $("#mobile").val();
	let phone = $("#phone").val();
	let email = $("#email").val();
	let dob = $("#dob").val();
	let street = $("#street").val();
	let city = $("#city").val();
	let state = $("#state").val();
	let country = $("#country").val();
	let zipcode = $("#zipcode").val();
	var config={
	  Entity:"Contacts",
	  APIData:{"id": zId,"First_Name": fname,"Last_Name": lname,"Mobile": mobile,"Phone": phone,"Email": email,"Date_of_Birth": dob,"Mailing_Street": street,"Mailing_State":state,"Mailing_City": city,
						"Mailing_Country": country,"Mailing_Zip": zipcode}
	}
	ZOHO.CRM.API.updateRecord(config)
	.then(function(data){})

	//update google contact by invoking api
	dob = new Date(dob);
	var names;
	if(fname != null){
		names = [{"givenName":fname,"familyName":lname}];
	}
	else{
		names = [{"givenName":lname}];
	}
	if(dob != null){
		birthdays = {"date":{"day":dob.getDate(),"month":dob.getMonth(),"year":dob.getFullYear()}};
	}
	let flag = false;
	var address;
	if(street != null){
			flag = true;
			address = {"streetAddress" : street};
	}
	if(state != null){
			flag = true;
			address["region"] = state;
	}
	if(city != null){
			flag = true;
			address["city"] = city;
	}
	if(country != null){
			flag = true;
			address["country"] = country;
	}
	if(zipcode != null){
			flag = true;
			address["postalCode"] = zipcode;
	}
	if(flag == true){
		var addresses = {address};
	}
	if(email != null){
		var emailAddresses = [{"value":email}];
	}
	if(mobile != null && phone != null){
		phoneNumbers = [{"type":"Mobile","value":mobile},{"type":"Phone","value":phone}];
	}
	else if(mobile != null){
		phoneNumbers = [{"type":"Mobile","value":mobile}];
	}
	else if(phone != null){
		phoneNumbers = [{"type":"Phone","value":phone}];
	}
	person = {"resourceName":"people/"+gId,"etag":etag,"metadata":{"objectType":"PERSON"},"names":names,"birthdays":birthdays,"addresses":addresses,"emailAddresses":emailAddresses,"phoneNumbers":phoneNumbers};
	param = {"resourceName" : "people/"+gId};
	param = {"person" : person};

	ZOHO.CRM.CONNECTOR.invokeAPI("synccontacts.gcontacts.updatecontacts",param)
	.then(function(data){
			console.log(data);
	})
	ZOHO.CRM.UI.Record.open({Entity:"Contacts",RecordID:entityId})
	.then(function(data){
	})

	ZOHO.CRM.UI.Popup.close()
	.then(function(data){})
}
